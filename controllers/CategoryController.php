<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 27.05.2018
 * Time: 20:39
 */

namespace app\controllers;
use app\models\Category;
use app\models\Product;
use Yii;
use yii\data\Pagination;

class CategoryController extends AppController
{

    public function actionIndex(){
        $hits = Product::find()->where(['hit' => 1])->limit(6)->all();
        $this->setMeta('E-SHOPPER');
        return $this->render('index',[
            'hits' => $hits
        ]);
    }

    public function actionView($id){
        $category = Category::findOne($id);
        if(empty($category)){
            throw new \yii\web\HttpException(404, 'The requested Category could not be found.');
        }

        $query = Product::find()->where(['category_id' => $id]);
        $pages = new Pagination(['totalCount' => $query->count(),'pageSize' => 3,
            'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $this->setMeta('E-SHOPPER | '.$category->name, $category->keywords, $category->description);
        return $this->render('view',[
            'products' => $products,
            'category' => $category,
            'pages' => $pages
        ]);
    }

    public function actionSearch(){
        $q = trim(Yii::$app->request->get('q'));

        $this->setMeta('E-SHOPPER | Search : '.$q);
        if(!$q){
            return $this->render('search');
        }
        $query = Product::find()->where(['like', 'name', $q]);
        $pages = new Pagination(['totalCount' => $query->count(),'pageSize' => 3,
            'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('search',[
           'products' => $products,
           'pages' => $pages,
           'q' => $q
        ]);

    }

}