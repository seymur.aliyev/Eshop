<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 19.06.2018
 * Time: 13:12
 */

namespace app\controllers;
use app\models\Product;
use app\models\Cart;
use Yii;
use app\models\Order;
use app\models\OrderItems;

class CartController extends AppController
{
    public function actionAdd(){
        $id = Yii::$app->request->get('id');
        $qty = (int)Yii::$app->request->get('qty');
        $qty = !$qty ?  1 : $qty;

        //product is object we pass into the cart model
        $product = Product::findOne($id);
        if(empty($product)) return false;
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->addToCart($product, $qty);
        if(!Yii::$app->request->isAjax){
            return $this->redirect(Yii::$app->request->referrer);
        }
        $this->layout = false;
        return $this->render('cart-modal',[
            'session' => $session
        ]);
    }

    public function actionClear(){
        $session = Yii::$app->session;
        $session->open();
        $session->remove('cart');
        $session->remove('cart.qty');
        $session->remove('cart.sum');
        $this->layout = false;
        return $this->render('cart-modal',[
            'session' => $session
        ]);
    }

    public function actionDelItem(){
        $id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->deleteItem($id);
        $this->layout = false;
        return $this->render('cart-modal',
            ['session' => $session]
        );
    }

    public function actionShow(){
        $session = Yii::$app->session;
        $session->open();
        $this->layout = false;
        return $this->render('cart-modal',['session' => $session]);
    }

    public function actionView(){
        //debug(Yii::$app->params['adminEmail']);die;
        $session = Yii::$app->session;
        $session->open();
        $this->setMeta('Cart');
        $order = new Order();
        if($order->load(Yii::$app->request->post())){

            $order->qty = $_SESSION['cart.qty'];
            $order->sum = $_SESSION['cart.sum'];
            if($order->save()){
                $this->saveOrderItems($session['cart'], $order->id);

                Yii::$app->mailer->compose('order',
                    ['session' => $session])
                    ->setFrom(['seymour.aliyev@yandex.ru' => 'Yii2'])
                    ->setTo($order->email)
                    ->setSubject('Order Response')
                    ->send();
                Yii::$app->session->setFlash('success','Order has been saved. Manager will contact u soon!');
                $session->remove('cart');
                $session->remove('cart.qty');
                $session->remove('cart.sum');
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error','Ups. Order has not been saved');
            }
        }

        return $this->render('view',
        [
            'orderModel' => $order,
            'session' => $session
        ]);
    }

    protected function saveOrderItems($items, $order_id){

        foreach ($items as $id => $item) {
            $order_items = new OrderItems();
            $order_items->order_id = $order_id;
            $order_items->product_id = $id;
            $order_items->name = $item['name'];
            $order_items->price = $item['price'];
            $order_items->qty_item = $item['qty'];
            $order_items->sum_item = $item['qty'] * $item['price'];
            $order_items->save(false);
        }
    }


}

