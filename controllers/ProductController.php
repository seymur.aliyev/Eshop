<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 07.06.2018
 * Time: 6:40
 */

namespace app\controllers;
use Yii;
use app\models\Category;
use  app\models\Product;


class ProductController extends AppController
{
    public function  actionView($id){
        $product = Product::findOne($id);
        if(empty($product)){
            throw new \yii\web\HttpException(404, 'The requested Product could not be found.');
        }
        $hits = Product::find()->where(['hit' => 1])->limit(5)->all();
        $this->setMeta('E-SHOPPER | '.$product->name, $product->keywords, $product->description);
        return $this->render('view',[
           'product' => $product,
           'hits' => $hits
        ]);
    }

}