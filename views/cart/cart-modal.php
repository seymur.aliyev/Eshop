<?php

use yii\helpers\Html;

if(!empty($session['cart'])):?>
        <div class="responsive-table">
            <table class="table table-hover table-striped">
                <head>
                    <tr>
                        <th>Foto</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>QTY</th>
                        <th>
                            <span class="glyphicon glyphicon-remove" aria-hidde="true"></span>
                        </th>
                    </tr>
                </head>
                <tbody>
                    <?php foreach ($session['cart'] as $id => $item):?>
                        <tr>
                            <td><?=Html::img("{$item['img']}",
                                    ['alt' => $item['name'],'height' => 50]);?></td>
                            <td><?=$item['name'];?></td>
                            <td><?=$item['price'];?></td>
                            <td><?=$item['qty'];?></td>
                            <td>
                                <span data-id="<?=$id;?>" class="glyphicon glyphicon-remove del-item text-danger" aria-hidde="true" ></span>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    <tr>
                        <td colspan="4">Qty:</td>
                        <td><?=$session['cart.qty'];?></td>
                    </tr>
                    <tr>
                        <td colspan="4">Sum:</td>
                        <td><?=$session['cart.sum'];?></td>
                    </tr>
                </tbody>

            </table>
        </div>
    <?php else : ?>
        <h2>Cart is empty</h2>

<?php endif; ?>


