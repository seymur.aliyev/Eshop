<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Order */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1>Order № <?= $model->id;?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'updated_at',
            'qty',
            'sum',
            //'status',
            [
                'attribute' =>'status',
                'value' => !$model->status ?
                   '<span class="text-danger">Active</span>'
                        : '<span class="text-success">Completed</span>'
                ,
                'format' => 'html',
            ],
            'name',
            'email:email',
            'phone',
            'address',
        ],
    ]) ?>


    <?php $items = $model->orderItems;?>
    <div class="responsive-table">
        <table class="table table-hover table-striped">
            <head>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>QTY</th>
                    <th>Sum</th>
                </tr>
            </head>
            <tbody>
            <?php foreach ($items as $item):?>
                <tr>
                    <td>
                        <a href="<?=Url::to(['/product/view','id'=> $item['id']])?>">
                            <?=$item['name'];?>
                        </a>
                    </td>
                    <td><?=$item['price'];?></td>
                    <td><?=$item['qty_item'];?></td>
                    <td><?=$item['sum_item'];?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <hr>
    </div>

</div>
